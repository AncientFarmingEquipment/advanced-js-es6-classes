class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set salary(newValue) {
        this._salary = newValue;
    }

    get salary() {
        return this._salary;
    }

    set name(newName) {
        this._name = newName;
    }

    get name() {
        return this._name
    }

    set age(newAge) {
        this._age = newAge;
    }

    get age() {
        return this._age;
    }
}

class Programmer extends Employee {
    constructor(lang, ...parentParams) {
        super(...parentParams);
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }

    set salary(newValue) {
        super.salary = newValue;
    }
}


const firstProgrammer = new Programmer('English', 'Steve Stifler', 16, 1000);
const secondProgrammer = new Programmer ('French', 'Napoleon Bonafarts', 33, 500);
const thirdProgrammer = new Programmer ('Deutsch', 'Amanda D. P. Throat', 33, 7600000000000000);
const fourthProgrammer = new Programmer ('Hindi', 'Mashtur Bates', 12, 0.1);
const fifthProgrammer = new Programmer ('Hebrew', 'Jesus Christ', 2022, 0);

console.log(firstProgrammer, secondProgrammer, thirdProgrammer, fourthProgrammer, fifthProgrammer)
